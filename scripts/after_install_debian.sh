#!/bin/bash

apt-get install curl vim emacs rofi neofetch exa gnome-terminal git

curl -fsSL https://starship.rs/install.sh | bash

git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d

~/.emacs.d/bin/doom install
